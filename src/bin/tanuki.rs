use tanuki::cli::Cli;

pub fn main() {
    // Make panics nicer
    human_panic::setup_panic!();

    Cli::run().expect("unexpected error");
}
