use clap::{Parser, Subcommand};
use std::path::PathBuf;

pub mod jsonnet;
pub mod gitops;
use jsonnet::JsonnetCommand;
use gitops::GitOpsCommand;


/// GitLab configuration surveyor
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    command: Commands,

    #[arg(global=true, short='o', long, default_value_t=ExportFormat::YAML)]
    /// Format to export the results in
    format: ExportFormat,
}

impl Cli {
    pub fn run() -> Result<(), Box<dyn std::error::Error>> {
	let cli = Self::parse();
	cli.command.run(&cli)
    }
}

/// Format to export any results within
#[derive(Debug, Clone, clap::ValueEnum)]
pub enum ExportFormat {
    JSON,
    ///
    YAML,
}

impl std::fmt::Display for ExportFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	match self {
	    ExportFormat::JSON => write!(f, "json"),
	    ExportFormat::YAML => write!(f, "yaml"),
	}
    }
}

#[derive(Subcommand, Debug)]
pub enum Commands {
    /// Read the
    Config {
	#[arg(short, long)]
	filename: Option<PathBuf>,
    },
    /// Export the registry information to
    Registry,
    /// Jsonnet related commands
    Jsonnet {
	#[clap(subcommand)]
	command: JsonnetCommand,
    },
    /// Manifest tanuki GitOps configuration commands
    #[clap(alias="go")]
    GitOps {
	#[clap(subcommand)]
	command: GitOpsCommand,
    },

}

impl Commands {
    fn run(&self, cli: &Cli) -> Result<(), Box<dyn std::error::Error>> {
	match self {
	    Commands::Config { filename: _ } => {

		// let a: crate::schema::service_catalog::ServiceCatalog = serde_yaml::from_star()?;
		Ok(())
	    },
	    Commands::Jsonnet { command } => command.run(cli),
	    Commands::GitOps { command } => command.run(cli),
	    _ => unimplemented!(),
	}
    }
}
