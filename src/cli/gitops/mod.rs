use clap::{Subcommand, Parser};

// TODO: move schema
mod schema;

#[derive(Subcommand, Debug)]
pub enum GitOpsCommand {
    /// Check for configuration errors
    Check {
	#[clap(flatten)]
	selector: ApplicationSelector,
    },
    /// Manifest an application
    Manifest,
    /// Get application status
    Status,
    /// Diff an application configuration with the given parameters
    Diff,
    /// Sync an application configuration with the given parameters
    Sync,
}

impl GitOpsCommand {
    pub fn run(&self, _cli: &crate::cli::Cli) -> Result<(), Box<dyn std::error::Error>> {
	match self {
	    GitOpsCommand::Check { selector } => {
		eprintln!("starting checking: {:?}", selector);
		eprintln!("finished checking");
		Ok(())
	    }
	    _ => unimplemented!(),
	}
    }
}

#[derive(Debug, Parser)]
pub struct ApplicationSelector {
    #[arg(short, long, global=true)]
    service: Option<String>,
    #[arg(short, long, global=true)]
    environment: Option<String>,
    #[arg(short, long, global=true)]
    release: Option<String>,
    #[arg(short='1', long, default_value_t=false)]
    unique: bool,
}
