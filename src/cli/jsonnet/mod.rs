use clap::{Subcommand};

#[derive(Subcommand, Debug)]
pub enum JsonnetCommand {
    /// Evaluate a single file
    Eval,
}

impl JsonnetCommand {
    pub fn run(&self, _cli: &crate::cli::Cli) -> Result<(), Box<dyn std::error::Error>> {
	unimplemented!()
    }
}
