pub trait Loader<T> {
    fn load()
}

pub struct FileLoader<T> {}

impl FileLoader<T> {
    pub fn load() -> T {

    }

    pub fn load_file(path: &Path) -> Result<String, Box<dyn std::error::Error>> {

    }

    pub fn load_url(url: &str) -> Result<String, Box<dyn std::error::Error>> {

    }
}

impl std::io::Reader for Loader<T> {

}
