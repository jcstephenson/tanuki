use std::collections::BTreeMap;
use serde::{Deserialize, Serialize};

/// Maps kubernetes cluster by name to configuration
// #[serde(Serialize, Deserialize)]
pub type KubernetesClusterMap = BTreeMap<String, KubernetesCluster>;

/// Kubernetes cluster definition
#[derive(Debug, Deserialize, Serialize)]
pub struct KubernetesCluster {
    pub name: String,
    pub project: String,
    pub region: String,
    /// Host used to connect to the cluster
    pub ssh_proxy: Option<String>,
}
