use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalog {
    pub tiers: Vec<ServiceCatalogTier>,
    pub services: Vec<ServiceCatalogService>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogTier {
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogService {
    pub name: String,
    pub friendly_name: String,
    pub tier: String,
    pub teams: Option<Vec<String>>,
    pub label: String,
    pub owner: String,
    pub business: Option<ServiceCatalogBusiness>,
    pub technical: ServiceCatalogTechnical,
    pub observability: Option<ServiceCatalogObservability>,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogBusiness {
    #[serde(rename="uppercase")]
    pub sla: Option<ServiceCatalogBusinessSLA>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogBusinessSLA {
    overall_sla_weighting: i64
}


#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogTechnical {
    pub project: Option<Vec<String>>,
    pub documents: Option<ServiceCatalogTechnicalDocuments>,
    pub dependencies: Option<Vec<ServiceCatalogDependency>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogTechnicalDocuments {
    pub design: Option<String>,
    pub architecture: Option<String>,
    pub readiness_review: Option<String>,
    pub sre_guide: Option<String>,
    pub developer_guide: Option<String>,
    pub service: Option<Vec<String>>,
    pub security: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogDependency {
    pub name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogLoggingEntry {
    pub name: String,
    pub permalink: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogObservability {
    pub monitors: ServiceCatalogMonitor,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServiceCatalogMonitor {
    pub primary_grafana_dashboard: Option<String>,
    pub sentry_slug: Option<String>,
    pub gitlab_dashboard: Option<String>,
}
